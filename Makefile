# Build public/ site and remove metadata from images
public: clean deps
	hugo

# Run hugo server
server: clean deps
	hugo server

# Dependencies
deps: /bin/hugo

clean:
	rm -rf .hugo_build.lock resources/ public/
