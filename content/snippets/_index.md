---
title: Snippets
---

Aquí guardo trozos de código como plantillas,
comandos con usos puntuales o trozos de código
para cuando necesito instalar o configurar algo.
