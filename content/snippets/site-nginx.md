---
title: Ejemplo configuración de sitio en nginx
date: 2023-10-30
---

### Configuración nginx

Guardar el siguiente archivo de configuración como `/etc/nginx/sites-available/example.org.conf`,
reemplazar `example.org` con el dominio deseado y `examplesite` con el directorio del sitio.


```nginx
server {
	listen 80;
	listen [::]:80;

	server_name example.org;

	root /var/www/examplesite;

	# Añadir index.php en caso de usar php
	index index.html;

	location / {
		try_files $uri $uri/ =404;
	}

    # Descomentar en caso de usar php
	#location ~ \.php$ {
	#	include snippets/fastcgi-php.conf;
	#	fastcgi_pass unix:/run/php/php7.4-fpm.sock;
	#}
}
```

### Activar el sitio

```shell
ln -s /etc/nginx/sites-available/example.org.conf /etc/nginx/sites-enabled/
systemctl reload nginx
```

### SSL/TLS

Certbot debería configurar automáticamente los certificados y la configuración de nginx

```shell
certbot --nginx
systemctl reload nginx
```
