---
title: Ejemplo 'reverse proxy' en nginx
date: 2023-10-30
---

La aplicación corriendo en el puerto `8080` en este caso, podrá ser visitada a través de internet
usando el dominio `example.org`, es posible redirigir subdominios.

Entonces, este método permite tener varias aplicaciones escuchando tráfico en diferentes puertos
y redirigir el tráfico de subdominios a las diferentes aplicaciones. Por ejemplo, redirigir
`git.example.org` a un servidor git, y `mumble.example.org` a un servidor de mumble.

### Configuración nginx

Guardar el siguiente archivo de configuración como `/etc/nginx/sites-available/example.org.conf`,
reemplazar `example.org` y `8080` con el dominio y puerto deseados.


```nginx
server {
	listen 80;
	listen [::]:80;

	server_name example.org;

	location / {
	    proxy_pass http://localhost:8080/;
	}
}
```

### Activar sitio

```shell
ln -s /etc/nginx/sites-available/example.org.conf /etc/nginx/sites-enabled/
systemctl reload nginx
```

### SSL/TLS

Certbot debería configurar automáticamente los certificados y la configuración de nginx

```shell
certbot --nginx
systemctl reload nginx
```
