---
title: ffmpeg tricks
date: 2023-11-27
---

# Stabilize videos

Shamefully stolen from [Paul Irish's blog](https://www.paulirish.com/2021/video-stabilization-with-ffmpeg-and-vidstab/).

```shell
# The first pass ('detect') generates stabilization data and saves to `transforms.trf`
# The `-f null -` tells ffmpeg there's no output video file
ffmpeg -i clip.mkv -vf vidstabdetect -f null -

# The second pass ('transform') uses the .trf and creates the new stabilized video.
ffmpeg -i clip.mkv -vf vidstabtransform clip-stabilized.mkv
```

# Extract audio from video

```shell
ffmpeg -i video.mp4 -vn -acodec copy audio-from-video.mp3
```

# Lower volume of audio track

```shell
ffmpeg -i audio.aac -af "volume=-15dB" audio-lower.aac
```

# Merge audio and video

```shell
ffmpeg -i video-only.mp4 -i audio-only.aac -codec copy -acodec aac video-merged.mp4
```

# Merge audio and video with audio

```
ffmpeg -y -i video-with-audio.mp4 -i audio-only.mp3 -filter_complex "[0:a][1:a]amix=duration=shortest[a]" -map 0:v -map "[a]" gameplay.mp4
```
