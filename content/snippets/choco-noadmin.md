---
title: Chocolatey sin permisos de administrador
date: 2023-10-23
---

### Guardar el siguiente script como `ChocolateyInstallNonAdmin.ps1`

```ps1
# Set directory for installation - Chocolatey does not lock
# down the directory if not the default
$InstallDir='C:\ProgramData\chocoportable'
$env:ChocolateyInstall="$InstallDir"

# If your PowerShell Execution policy is restrictive, you may
# not be able to get around that. Try setting your session to
# Bypass.
Set-ExecutionPolicy Bypass -Scope Process -Force;

# All install options - offline, proxy, etc at
# https://chocolatey.org/install
iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

Tomado de la [documentación de Chocolatey](https://docs.chocolatey.org/en-us/choco/setup#non-administrative-install)

### En PowerShell:

```ps1
Set-ExecutionPolicy Bypass -Scope Process -Force;
.\ChocolateyInstallNonAdmin.ps1
```

### Algunos programas disponibles:

```ps1
choco install git.commandline
choco install vim-tux.portable
choco install groff
```
