---
title: Quitar fondo de una imagen con image-magick
date: 2023-11-13
---

`white` podría ser también un hex code.

```shell
convert -transparent white -fuzz 10% image.jpg image-transparent.png
```

Cambiar el color `#076678` de una imagen y reemplazarlo por `#121212`

```shell
convert image.jpg -fuzz 15% -fill "#121212" -opaque "#076678" image-swapped.jpg
```
