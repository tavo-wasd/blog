---
title: Word Lists
date: 2023-12-05
---

Las mantengo acá para accesarlas desde cualquier
lado y modificarlas en un solo lugar. Son útiles
para generar nombres de usuario randomizados, por
ejemplo.

# Animals

Disponible en: [animals.txt](wordlists/animals.txt)

```
aardvark
african-elephant
african-tree-pangolin
albatross
alligator
alpaca
.
.
.
```

# Adjectives

Disponible en: [adjectives.txt](wordlists/adjectives.txt)

```
aback
abaft
abandoned
abashed
aberrant
abhorrent
.
.
.
```
