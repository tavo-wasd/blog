---
title: "Guía rápida para groff"
date: 2023-11-16
---

[Plantilla resumida](plantilla)

# Formato general de página y contenido

```groff
.ds FAM T \" H=Helvetica C=Courier
.nr PS 10 \" Tamaño de letra
.nr VS 12 \" Espaciado
.nr PO 1i \" Espaciado borde izquierdo
.nr LL 6i \" Longitud de línea
.nr LT 6i \" Longitud de encabezado y pie de página
.nr HM 1i \" Margen del encabezado
.nr FM 1i \" Margen del pie de página
```

Por ejemplo, utilizando:

```groff
.ds FAM T \" Tipo de letra
.nr PS 12 \" Tamaño de letra
.nr VS 20 \" Espaciado
.TL
Título
.AU
Autor
.
.LP
Lorem ipsum dolor sit amet, consectetur...
```

Produce:

![ejemplo-times](times.png)

... y utilizando:

```groff
.ds FAM H \" Tipo de letra
.nr PS 12 \" Tamaño de letra
.nr VS 20 \" Espaciado
.TL
Título
.AU
Autor
.
.LP
Lorem ipsum dolor sit amet, consectetur...
```

Produce:

![ejemplo-helvetica](helvetica.png)

# Primera página

```groff
.TL
<título>
.AU
<autor>
.AU
<otro autor>
.AI
<institución>
.
.AB
<abstract>
.AE
```

Por defecto, groff empezará el documento justo después
del RESUMEN o ABSTRACT, o si no existe, después del autor
o el título. Para utilizar una **portada**, se pueden utilizar
las siguientes directivas:

```groff
.RP no \" 'Report page' o portada.
.P1    \" Empezar numeración en página 1
```

# Secciones y tabla de contenido

```groff
.NH
.XN "<sección numerada>"
.
.NH 2
.XN "<subsección numerada>"
.
.SH
.XN "<sección sin numerar>"
.
.TC \" Tabla de contenido al final del documento
```

Por defecto, groff pone la table de contenido al final de la página.
Para ponerla al inicio, o después de una página de portada, se puede utilizar
la siguiente macro:

```groff
.\" github.com/SudarsonNantha
.\" ---
.\" Relocate .TC at the end of the document to either 
.\" 'before' or 'after' (by giving those arguments)
.\" the page where this macro was sourced.
.\" ---
.\" Requires -mspdf macros
.
.if '\*[.T]'pdf' \X'pdf: pagename here'
.rn TC TCold
.de TC
.   ds CF
.   ds CH
.   if '\*[.T]'pdf' \X'pdf: switchtopage \\$1 here'
.   TCold
..
```

Para utilizarla, se agrega el código anterior al inicio del
documento y al momento de llamar `.TC` al final del
código, se puede cambiar la directiva por `.TC after`
para poner la tabla de contenidos después de la portada, o
`.TC before` para ponerla antes de la portada.

![ejemplo-tablacontenido](tablacontenido.png)

# Párrafos

```groff
.PP
<párrafo con \f[CW]sangría\f[] \m[blue]con\m[] un \f[B]poco\f[] de \f[I]formato\f[]>
.
.LP
<párrafo sin \f[CW]sangría\f[] \m[red]con\m[] un \f[B]poco\f[] de \f[BI]formato\f[]>
```

![ejemplo-parrafo](parrafo.png)

# Matemáticas

Las siguientes definiciones en `eqn`, permiten el
uso de sintaxis simplificado a la hora de crear ecuaciones.
(El `delim $$` funciona para poner syntax matemático dentro del texto,
similar a LaTeX).

```groff
.EQ
delim $$
define / 'over'
define <( '{ \[la] }'
define )> '{ \[ra] }'
define grad '{ \[gr] }'
define lag '\f[U-HR]\[u2112]'
.EN
```

Por ejemplo, la expresión:

```groff
.EQ
\[gr] \f[U-HR]\[u2112] ( lambda , x , y ) =
    \[la]
        {partial lag} over {partial lambda} ,
        {partial lag} over {partial x} ,
        {partial lag} over {partial y}
    \[ra]
.EN
```

Puede ser simplificada como (después de aplicar las definiciones anteriores):

```groff
.EQ
grad lag ( lambda , x , y ) =
    <(
        {partial lag} / {partial lambda} ,
        {partial lag} / {partial x} ,
        {partial lag} / {partial y}
    )>
.EN
```

Para producir:

![ejemplo-eqn](eqn.png)

# Otros idiomas

groff usa variables para guardar los nombres de
la tabla de contenido, o las referencias. Para
cambiarlos, se modifican las variables así:

```groff
.\" Redefinición de registros comunes
.\" para títulos y fechas en español.
.
.ds ABSTRACT RESUMEN
.ds TOC Tabla de contenido
.ds REFERENCES Referencias
.
.ds MONTH1 enero
.ds MONTH2 febrero
.ds MONTH3 marzo
.ds MONTH4 abril
.ds MONTH5 mayo
.ds MONTH6 junio
.ds MONTH7 julio
.ds MONTH8 agosto
.ds MONTH9 septiembre
.ds MONTH10 octubre
.ds MONTH11 noviembre
.ds MONTH12 diciembre
.
.ds DY \n[dy] de \*[MO] de \n[year]
```

# Utilidades

## Regla

```groff
.de rule
\D'l \\$1 0'
..
```

Definiendo esta macro, se puede utilizar la directiva
`.rule LONGITUD` para dibujar una línea en el documento,
por ejemplo después de un título (el espaciado es para
evitar que el título tome la longitud de la línea):

```groff
.NH
.XN "\s'20'Título de ejemplo"
.sp 0i \" Espaciado de 0
.rule 6.25i
```

El `\s'20'` pone el tamaño de letra en 20, y produce:

![ejemplo-regla](regla.png)

# Avanzado

## Instalar fuente Computer Modern (LaTeX) de AMS Fonts

```shell
apt install cm-super
```
