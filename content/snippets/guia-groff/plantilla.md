---
title: "Plantilla groff_ms"
date: 2023-11-16
---

```groff
.\" .ds FAM T       \" H=Helvetica C=Courier
.\" .nr PS 10p      \" Point size
.\" .nr VS 12p      \" Line spacing
.\" .so report.tmac \" Cover page
.\" .so utils.tmac  \" General utils
.\" .so toc.tmac    \" Relocate toc
.\" .so md.tmac     \" Md-like sections
.\" .so math.tmac   \" Math utils
.\" .so es.tmac     \" Spanish
.
.TL
<title>
.AU
<author>
.AI
<institution>
.
.AB
<abstract>
.AE
.
.NH
.XN "<numbered heading>"
.
.PP
<indented \f[CW]paragraph\f[] \m[blue]with\m[] \f[B]some\f[] \f[I]formatting\f[]>
.
.\" .TC
```
