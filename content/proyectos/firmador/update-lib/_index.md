---
title: "update-lib.sh"
date: 2024-05-12
---

# Script para actualizar librerías automáticamente

- 0 notificaciones de `shellcheck` en modo `POSIX sh`
- Solamente comandos estrictamente necesarios y utilidades/funciones de `POSIX sh (printf, while, case)`

## 1. Revisar comandos externos a shell, definir funciones para loggear

```sh
#!/bin/sh
# External dependencies:
deps="curl tar cd mkdir cmake make"
script_name="$0"

success() {
    printf '    %s - \033[32mSuccess:\033[0m \033[2m%s\033[0m\n' "$script_name" "$1"
}

warn() {
    printf '    %s - \033[33mWarn:\033[0m \033[2m%s\033[0m\n' "$script_name" "$1"
}

consolelog() {
    printf '    %s - \033[2mLog: %s\033[0m\n' "$script_name" "$1"
}

error() {
    printf '    %s - \033[31mError: %s\033[0m\n' "$script_name" "$1"
}

missing_deps="$(for dep in $deps ; do
    ! command -v "$dep" 1>/dev/null 2>&1 && printf '%s ' "$dep"
done)"

[ -n "$missing_deps" ] && error "Missing dependencies for $script_name: $missing_deps" && exit 1

...
```

## 2. Función para comprobar la última versión de wxWidgets

```sh
get_latest_wx() {
    url="https://api.github.com/repos/wxWidgets/wxWidgets/releases/latest"

    current_wx="$(for wx in lib/wx/wxWidgets-* ; do printf '%s' "$wx" ; done)"
    current_wx="${current_wx##*-}"
    current_wx="${current_wx%.tar.bz2}"

    latest_wx_url="$(curl -s "$url" | while IFS= read -r line ; do
        case "$line" in *browser_download_url*)
            url="${line%\"*}" ; url="${url##*\"}" ; file="${url##*/}"
            [ "${file##*.}" = "bz2" ] && [ "${file%-*}" = "wxWidgets" ] &&
                printf '%s\n' "$url" && break ;;
        esac
    done)"
    
    mkdir -p lib/wx
    file="${latest_wx_url##*/}"
    latest_wx_dir="lib/wx/${file%%.tar.bz2}"
    latest_wx="${latest_wx_dir##*-}"

    # Nor file nor new version can be determined
    [ -z "$file" ] || [ -z "$latest_wx" ] && return 1
    
    # wx is up to date, 0 if success
    [ "$latest_wx" = "$current_wx" ] &&
        success "wxWidgets-$current_wx is the latest version" &&
        return 0

    ...
```

## ... o descargarla de lo contrario

```sh
    # Download wx if not present or outdated, 0 if success 1 if fail
    warn "wxWidgets-$current_wx is behind wxWidgets-$latest_wx, downloading..."
    curl -sLO "$latest_wx_url" &&
    curl -sL "$latest_wx_url" -o "lib/wx/$file" &&
        consolelog "Extracting $file..." && (cd lib/wx && tar xf "$file") &&
        consolelog "Deleting archive $file..." && rm -f "lib/wx/$file" &&
	return 0 || return 1
}

...
```

## 3. Función para construir wx-config

```sh
build_wx() {
    mkdir -p "$latest_wx_dir/autobuild"
    (cd "$latest_wx_dir/autobuild" && cmake .. && make) 1>/dev/null || return 1
}

...
```

## 4. Ejecución
- Buscar última versión de librerías (únicamente wxWidgets por ahora)
- Compilarlas si es necesario
- Loggear y retornar códigos correctos

```sh
! get_latest_wx && error "Checking, downloading, or extracting failed." && exit 1

if [ "$latest_wx" != "$current_wx" ] ; then
    consolelog "Building wxWidgets..."
    ! build_wx && error "Building wxWidgets failed" && exit 1
    success "Built wxWidgets"
else
    binary="$(find ./ -name 'wx-config')"
    [ -n "$binary" ] && success "Found $binary, skipping building wxWidgets"
    if [ -z "$binary" ] ; then
        consolelog "Building wxWidgets..."
        ! build_wx && error "Building wxWidgets failed" && exit 1
        binary="$(find ./ -name 'wx-config')"
        [ -n "$binary" ] && success "Built wxWidgets"
        [ -z "$binary" ] && error "Can't build wxWidgets" && exit 1
    fi
fi
```

Link del script en texto plano: [update-lib.sh](update-lib.sh.txt)
