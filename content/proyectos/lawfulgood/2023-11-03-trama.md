---
title: "Trama"
date: 2023-11-03
---

# Plot
- En inglés porque creo que es mejor empezarlo
así y luego traducirlo a otros idiomas.

## Exposition

MAINCHARACTER is a regular law-abiding citizen living
in a dystopian future, where crime is at an all time
high. Governments control people through propaganda,
threats, power demonstration and market manipulation.
The proletariat while being the largest class, are also
the poorest and last in political relevance.

At the very beginning, MAINCHARACTER is boarding the
train to a marine attraction where he/she was supposed
to meet his/her "TENDR" match. However, after arriving
a few seconds late and watching the train leave, then
getting an unappealing text from the match, decides
to [go anyway]/[go back home].

Before taking an old-school taxi again, calls the new
rocket fueled taxi service. Then, learns one can also
visit other planets or regions within those that one
can't normally visit with public transport services.
In the middle of booking a trip, corrupt police arrests
the taxi driver for illegally transporting people from
the cities to areas "unprotected" by the government.
MAINCHARACTER [tells on]/[saves] the taxi driver, which
causes him to be [arrested]/[set free], and MAINCHARACTER
to be offered a [police academy]/[street fights] pamphlet.

## Rising action

ENEMY helps MAINCHARACTER reach a high position in
the state (police, politics, cybersecurity, health).
But could also make MAINCHARACTER to join an unlawful
group (drug trafficking, fraud, cybercrime). ENEMY is
also a law-abiding citizen, but one who would never
conspire against the state. Eventually, both views
oppose at some degree. This indirectly triggers an
event that could potentially destroy the earth.

In the middle of visiting other planets, MAINCHARACTER
watches a live stream where earth is subject to a
nuclear war and the surface becomes hell in minutes.
As FRIEND who was suspect of being the VILLAIN tries
to escape, MAINCHARACTER watches him die, and the true
VILLAIN, ENEMY is revelaed. ENEMY thinks she is doing
this "sacrifices" for the greater good, MAINCHARACTER
also watches as "the elite" argue in a table game as
if they didn't just commit genocide. Just when MAINCHARACTER
realizes he/she has a gun and could terminate one or many
corrupt leaders, he/she is mugged and the great leaders
pay the bar security to get everyone out since they can't
concentrate.

## Climax

TBD

## Falling action

TBD

## Resolution

TBD

