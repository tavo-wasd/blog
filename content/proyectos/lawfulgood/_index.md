---
title: "Lawful good (game)"
date: 2023-11-03
draft: true
---

No tengo mucho más que el título, una trama básica y algunos personajes.
De momento no tengo mucho qué publicar aquí pero seguramente vaya subiendo
el progreso poco a poco. A corto plazo tal vez una sinopsis medio definida
no estaría mal. En general me gustaría que fuera un juego técnicamente sencillo,
pero con bastante contenido.
