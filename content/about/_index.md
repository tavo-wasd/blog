---
title: Acerca de tavo.one
---

"I... a universe of atoms, an atom in the universe" 🌠

- Email: [tavo@tavo.one](mailto:tavo@tavo.one)
- PGP: [tavo@tavo.one.asc](/pgp.txt)
- RSS: [index.xml](/index.xml)
