---
title: "Guía de instalación de archlinux"
date: 2024-12-27
---

# Bootear el entorno y preparar el sistema

En caso de tener un teclado con una disposición distinta a `us`:

```sh
localectl list-keymaps # Listar keymaps disponibles
loadkeys mi-keymap     # Cargar un keymap
```

## Particionar discos

```sh
# listar los discos:
# fdisk -l

fdisk /dev/sdx
```

La configuración mínima para UEFI son dos particiones:

- Partición que será montada en `/boot` de al menos `1G`
- Partición que será montada en `/` de al menos `23G`

NOTA: En el resto de la guía, se asume que las particiones

- `sdx1` es la partición EFI que será montada en `/boot`
- `sdx2` será montada en `/`

## Encripción (opcional)

```sh
cryptsetup luksFormat /dev/sdx2
cryptsetup open /dev/sdx2 cryptlvm
```

Necesitaremos los UUIDs de la partición encriptada y de cryptlvm para más adelante, es buena idea
guardarlos:

```sh
# El primero es crypto_LUKS y el segundo es la partición root desencriptada
lsblk -f | grep 'crypto_LUKS\|cryptlvm' | awk '{print $4}'
```

## Formato

```sh
mkfs.ext4 /dev/sdx2
mkfs.fat -F 32 /dev/sdx1
```

## Montar

```sh
mount /dev/sdx2 /mnt
# mount /dev/mapper/cryptlvm /mnt # <- En caso de usar encripción
```

```sh
mkdir -p /mnt/boot
mount /dev/sdx1 /mnt/boot/
```

## Pacstrap

```sh
pacstrap -K /mnt base linux linux-firmware grub sudo networkmanager efibootmgr

# No olvidar el editor de texto, por ejemplo vim o nano
pacstrap -K /mnt vim

# pacstrap -K /mnt cryptsetup lvm2 # <- En caso de usar encripción
```

# Configurar el sistema

```sh
genfstab -U /mnt >> /mnt/etc/fstab
```

A partir de ahora, estaremos dentro del sistema que estamos instalando, con el comando:

```sh
arch-chroot /mnt
```

## Locale

```sh
# ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
ln -sf /usr/share/zoneinfo/America/Costa_Rica /etc/localtime
```

Luego:

```sh
hwclock --systohc
```

Editar `/etc/locale.gen` y descomentar las locales requeridas, por ejemplo:
`#es_CR.UTF-8 UTF-8`

Luego, generarlas con:

```sh
locale-gen
```

Crear el archivo `/etc/locale.conf` y configurar el lenguaje agregando la línea
(o cualquier otra locale de preferencia):

```sh
LANG=es_CR.UTF-8
```

## Redes

Definir el hostname en `/etc/hostname`, escribiendo por ejemplo: `arch-pc`
o el hostname deseado.

Configurar `/etc/hosts`, una configuración mínima de ejemplo:

```
127.0.0.1       localhost
127.0.1.1       arch-pc # (el nombre definido en /etc/hostname)
::1             localhost
```

```sh
systemctl enable NetworkManager
```

## Usuarios

```sh
useradd -G -m wheel mi-usuario # utilizar el nombre de preferencia
passwd mi-usuario              # definir la contraseña del usuario
```

Ejecutar `visudo` y descomentar la línea para habilitar a miembros de "wheel" usar "sudo", (o en su
defecto, agregarla manualmente):

```
# %wheel ALL=(ALL:ALL) ALL
```

(Opcional) Comprobar que se puede utilizar el comando `sudo` cuando se usa el nuevo
usuario:

```sh
su - mi-usuario
sudo whoami # debería imprimir: root
exit # volver al usuario root
```

(Opcional) Bloquear usuario `root` por seguridad:

```sh
passwd -l # correr como usuario root
```

## Initramfs

(Opcional, en caso de habilitar encripción) Editar el archivo en
`/etc/mkinitcpio.conf` y agregar las siguientes HOOKS:

```sh
# Asegurarse que estén en este orden y antes de "filesystems" y "fsck"
HOOKS=(... encrypt lvm2 ...)
```

Crear un entorno de disco ram inicial

```sh
mkinitcpio -P
```

## Bootloader

Asumiento que se utiliza GRUB:

(Opcional, en caso de usar encripción) Modificar esta línea en
`/etc/default/grub`:

```sh
...
# cryptdevice es crypto_LUKS y root es la partición root desencriptada
GRUB_CMDLINE_LINUX_DEFAULT="... cryptdevice=UUID=00000000-0000-0000-0000-000000000000:cryptlvm root=UUID=00000000-0000-0000-0000-000000000000"
...
```

Generar configuración de GRUB

```sh
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
grub-mkconfig -o /boot/grub/grub.cfg
```

## Finalizar

Salir de `chroot` y reiniciar

```sh
exit
reboot
```

# (Opcional) Post-install

Algunas opciones extras en caso de requerir audio, drivers de nvidia, o un
entorno de escritorio.

## Audio

Suponiendo un sistema con `pipewire` (el servidor multimedia de preferencia
actualmente)

```sh
pacman -S pipewire pipewire-alsa pipewire-pulse pipewire-jack wireplumber
```

## Nvidia

```sh
pacman -S nvidia nvidia-utils lib32-nvidia-utils
```

Hasta la fecha actual (2024-12-27), es necesario agregar estas opciones para
utilizar un chip de nvidia junto con el driver propietario.

Modificar esta línea en `/etc/default/grub`

```sh
...
GRUB_CMDLINE_LINUX_DEFAULT="... nvidia-drm.modeset=1 nvidia_drm.fbdev=0"
...
```

Agregar/Modificar esta línea en `/etc/mkinitcpio.conf`

```sh
...
MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)
...
```

## GUI

```sh
# KDE Plasma
pacman -S plasma-meta    # Instalar todo el entorno
pacman -S plasma-desktop # Instalar la base solamente

# GNOME
pacman -S gnome gnome-extra # Instalar todo el entorno
pacman -S gnome             # Instalar la base solamente

# i3
pacman -S i3-wm

# Qtile
pacman -S qtile
```
