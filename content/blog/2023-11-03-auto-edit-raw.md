---
title: "Usando darktable-cli para minimizar el tiempo invertido editando fotos"
date: 2023-11-03
---

En general, intento usar herramientas que me ayuden a agilizar
el proceso de editar fotografías porque en muchas ocasiones se
puede durar hasta semanas editando sesiones de fotos.

Entonces
busqué una manera fácil de aplicar ajustes a múltiples fotografías
crudas rápidamente y de preferencia en la CLI.
Claro que para resultados más complejos o artísticos se requiere
editar cada foto por separado, pero para sesiones de eventos o con
condiciones replicables, es muy conveniente usar LUTs o copiar
el historial de ajustes entre fotos.

En este caso, me descargué un pack de LUTs y creé estilos que
aplicaban esos LUTs y un par de ajustes base. Luego busqué en
el manual de Darktable cómo aplicar estos estilos utilizando
la CLI. Al final logré aplicar las opciones que necesitaba en
este script:

```shell
#!/bin/sh
FOLDER="$1"
STYLE="$2" # moody_bw, teal

# Configs
EXTENSION="jpg"
LONG_EDGE=1920
QUALITY=89

darktable-cli \
    --import "$FOLDER" \
    --width "$LONG_EDGE" \
    --height "$LONG_EDGE" \
    --out-ext ".$EXTENSION" \
    --style "$STYLE" \
    "${FOLDER%%/}/darktable_export/IMG.jpg" \
    --core --conf plugins/imageio/format/jpeg/quality=$QUALITY \
```

Básicamente limita la resolución de las imágenes, les aplica
el estilo que ya está guardado o configurado, y las exporta con
la calidad especificada. Obviamente para usarlo de necesita el
programa `darktable`, y crear un estilo. El nombre de ese estilo
es el segundo argumento.

Hay un par de ejemplos en la [sección de fotos](https://tavo.one/fotos/).
