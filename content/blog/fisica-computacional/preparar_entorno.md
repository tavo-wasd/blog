---
title: "1. Preparar el entorno"
date: 2024-11-14
---

## Descargar ISOs

- [Debian (live)](http://mirrors.ucr.ac.cr/debian-cd/current-live/amd64/iso-hybrid/)
- [Windows 10](https://www.youtube.com/watch?v=dQw4w9WgXcQ)

## Virtualizar sistema operativo

- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- [Hyper-V (Exclusivo Windows)](https://learn.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v)
- [Boxes (Exclusivo Linux)](https://flathub.org/apps/org.gnome.Boxes)

## (Opcional) Instalar sistema operativo

Tutorial: [Tutorial para Instalar un sistema operativo]()

1. Crear USB booteable ([Balena Etcher](https://etcher.balena.io/))
2. Seleccionar USB en BIOS/Boot Menu (dependiendo de la computadora)
3. Seguir pasos de instalación (dependiendo del sistema operativo a instalar)
