---
title: "4. git"
date: 2024-11-14
---

```sh
mkdir -p ~/.config/git
vim ~/.config/git/config
```

Vamos a crear un archivo de configuración base para git. No es de mucha
relevancia el usuario e email que escribamos aquí, funciona más que nada como
referencia para saber quién hizo un cambio. Por ejemplo, aquí podría poner
`name = compu1` para identificar que un cambio fue hecho desde la computadora 1.
El archivo debería verse así:

```toml
[user]
	email = miusuario@correo.com
	name = usuario
```

Ahora, necesitamos una forma de autenticarnos en un servidor corriendo `git`.
Primero, necesitamos crear una llave SSH:

```sh
ssh-keygen -t ed25519
# Va a pedir nombre de la llave, y una contraseña, nada de esto es necesario.
# Podemos simplemente presionar ENTER hasta que se cree la llave.
```

Ahora, necesitamos agregar esta llave en la plataforma de preferencia.
Primero, la imprimimos en la terminal y la copiamos (usualamente con
`Ctrl+Shift+V`)

```sh
cat ~/.ssh/id_ed25519.pub
```

Para agregarla en [GitHub](https://github.com/settings/keys), pegamos la llave
en la pestaña _Ajustes > Llaves SSH y GPG_.
