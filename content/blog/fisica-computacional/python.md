---
title: "5. Python"
date: 2024-11-14
lastmod: 2024-12-25
---

El procedimiento de descargar python varía de acuerdo al sistema operativo que
estemos utilizando, para instalarlo en Debian GNU/Linux (y derivados como
Ubuntu):

```sh
sudo apt install python3 python-is-python3 python3-dev python3-pip python3-venv
```

Necesitaremos varias librerías base para poder trabajar con Python, para
obtener las librerías de forma ordenada podemos:

**1. Preparar un entorno virtual "base"**

```sh
# Este comando:
# Crea un entorno virtual y lo guarda en ~/.local/share/venv/base

python -m venv ~/.local/share/venv/base
```

**2. Llamar al entorno de manera sencilla**

```sh
# OPCIÓN 1
# Agrega una instrucción al bashrc que llama al script activate para activar el
# entorno virtual (como está en el bashrc, correrá automáticamente en nuevas
# instancias de Bash)

echo ". ~/.local/share/venv/base/bin/activate" >> ~/.bashrc

# OPCIÓN 2
# En lugar de siempre activar el mismo entorno, podemos crear un alias "venv"
# que al escribirse activa el entorno "base". Es decir, cada vez que vayamos
# a trabajar en un proyecto de python, escribimos "venv".

echo 'alias venv=". ~/.local/share/venv/base/bin/activate"' >> ~/.bashrc
```

**3. Instalar librerías**

Una vez tengamos activado el entorno, podemos empezar a instalar los paquetes
necesarios para el curso.

```sh
pip install --upgrade pip          # Actualizar pip a la versión más reciente
pip install jupyter                # Podemos instalar un paquete
pip install numpy matplotlib scipy #   ... o varios a la vez
pip install mkdocs mkdocs-material
```

# Opción automatizada: `pyv`

Con esta herramienta podemos abstraer la funcionalidad del módulo de
python-venv que se utiliza anteriormente para el manejo de librerías.

Página del proyecto `pyv`: [git.tavo.one/tavo/pyv](https://git.tavo.one/tavo/pyv)

Tutorial completo sobre `pyv`:

{{< rawhtml >}}
<video width=100% controls>
    <source src="https://r2.tavo.one/blog/pyv.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>
{{< /rawhtml >}}

**Instalar `pyv`**

```sh
git clone https://git.tavo.one/tavo/pyv.git ~/.local/share/pyv
printf 'for f in pyv pyv_comp.bash ; do if [ -f ~/.local/share/pyv/$f ] ; then . ~/.local/share/pyv/$f ; fi ; done' >> ~/.bashrc
```

Si tenemos la herramienta `pyv` instalada, podemos abstraer esta funcionalidad.
Por ejemplo, con los siguientes comandos:

```sh
pyv new v1   # create venv named v1
pyv new v2   # create venv named v2
pyv ls       # list venvs
pyv rm v2    # delete venv named v2
pyv enter v1 # enter venv named v1
pyv exit     # exit current venv
```
